const { _, log } = require('basd')
const MockDb = require('@plaindb/storage/mock')
const Log = require('../lib/log')

describe('Log', () => {
  let logg
  let db

  beforeEach(async () => {
    db = new MockDb()
    logg = new Log(db)
    await logg.start()
  })

  it('should initialize with empty length', () => {
    expect(logg.length).to.equal(0)
  })

  describe('#append', () => {
    it('should append a value and increase length', async () => {
      await logg.append('value1')
      expect(logg.length).to.equal(1)
    })
  })

  describe('#push', () => {
    it('should push a value and increase length', async () => {
      await logg.push('value2')
      expect(logg.length).to.equal(1)
    })
  })

  describe('#shift', () => {
    it('should shift a value and decrease length', async () => {
      await logg.push('value3')
      await logg.push('value4')
      const value = await logg.shift()
      expect(value).to.equal('value3')
      expect(logg.length).to.equal(1)
    })
  })

  describe('#pop', () => {
    it('should pop a value and decrease length', async () => {
      await logg.push('value5')
      const value = await logg.pop()
      expect(value).to.equal('value5')
      expect(logg.length).to.equal(0)
    })
  })

  describe('#splice', () => {
    it('should splice values correctly', async () => {
      await logg.push('value6')
      await logg.push('value7')
      await logg.push('value8')
      await logg.splice(1, 1, 'value9')
      const value = await logg.get(1)
      expect(value).to.equal('value9')
      expect(logg.length).to.equal(3)
    })
  })

  describe('#stream', () => {
    it('should stream values correctly', async () => {
      await logg.push('value8')
      await logg.push('value9')
      const stream = logg.stream()
      const results = []
      stream.on('data', data => results.push(data))
      await new Promise(resolve => stream.on('end', resolve))
      expect(results).to.deep.equal(['value8', 'value9'])
    })
  })
})
