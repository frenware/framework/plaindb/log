# Log

[![npm](https://img.shields.io/npm/v/@plaindb/log?style=flat&logo=npm)](https://www.npmjs.com/package/@plaindb/log)
[![pipeline](https://gitlab.com/frenware/framework/plaindb/log/badges/master/pipeline.svg)](https://gitlab.com/frenware/framework/plaindb/log/-/pipelines)
[![license](https://img.shields.io/npm/l/@plaindb/log)](https://gitlab.com/frenware/framework/plaindb/log/-/blob/master/LICENSE)
[![downloads](https://img.shields.io/npm/dw/@plaindb/log)](https://www.npmjs.com/package/@plaindb/log)

[![Gitlab](https://img.shields.io/badge/Gitlab%20-%20?logo=gitlab&color=%23383a40)](https://gitlab.com/frenware/framework/plaindb/log)
[![Twitter](https://img.shields.io/badge/@basdwon%20-%20?logo=twitter&color=%23383a40)](https://twitter.com/basdwon)
[![Discord](https://img.shields.io/badge/Basedwon%20-%20?logo=discord&color=%23383a40)](https://discordapp.com/users/basedwon)

A lightweight package that transforms any key-value database into an indexed, log-structured database. This wrapper allows you to perform array-like operations—such as `push`, `pop`, `shift`, and `splice`—on your data, treating the database as an ordered list of entries where each entry comes with a unique sequence number. Ideal for scenarios where the order of data entries matters, this package is versatile enough to integrate into various projects requiring structured, indexed data storage.

## Features

- Lightweight and fast
- Key encoding using 'lexint'
- Built-in iterator for easy data manipulation
- Asynchronous API
- Supports common array operations like `push`, `pop`, `shift`, and `splice`

## Installation

Install the package with:

```bash
npm install @plaindb/log
```

## Usage

First, import the `Log` library.

```js
import Log from '@plaindb/log'
```
or
```js
const Log = require('@plaindb/log')
```

### Initialize

```javascript
const logDB = new Log(dbInstance)
await logDB.start()
```

Or use the static `start` method:

```javascript
const logDB = await Log.start(dbInstance)
```

### Append Data

```javascript
await logDB.append({ key: 'value' })
```

### Push Data

```javascript
await logDB.push({ anotherKey: 'anotherValue' })
```

### Shift Data

```javascript
const shiftedValue = await logDB.shift()
```

### Pop Data

```javascript
const poppedValue = await logDB.pop()
```

### Get Data by Sequence

```javascript
const value = await logDB.get(sequence)
```

### List Data

```javascript
const list = await logDB.list({ limit: 10, reverse: true })
```

### Iterate Through Data

```javascript
const iter = logDB.iterator({ gt: 10 })
for await (const item of iter) {
  console.log(item)
}
```

### Splice Data

```javascript
await logDB.splice(5, 2, { newItemKey: 'newValue' })
```

## Documentation

- [API Reference](/docs/api.md)

## API

### Class: `Log`

#### Constructor: `new Log(db)`

- `db`: The database instance to be used.

#### Static Methods:

- `static start(...args)`: Asynchronously initialize the log DB.

#### Instance Methods:

- `append(value)`: Appends a value to the log.
- `push(value)`: Appends a value to the log.
- `shift()`: Removes and returns the first value.
- `pop()`: Removes and returns the last value.
- `get(seq)`: Retrieves a value by its sequence.
- `list(opts)`: List the logs based on options.
- `iterator(opts)`: Returns an async iterator for log items.
- `splice(start, delCount, ...items)`: Similar to Array.prototype.splice but for the log.

## Tests

In order to run the test suite, simply clone the repository and install its dependencies:

```bash
git clone https://gitlab.com/frenware/framework/plaindb/log.git
cd log
npm install
```

To run the tests:

```bash
npm test
```

## Contributing

Thank you! Please see our [contributing guidelines](/docs/contributing.md) for details.

## Donations

If you find this project useful and want to help support further development, please send us some coin. We greatly appreciate any and all contributions. Thank you!

**Bitcoin (BTC):**
```
1JUb1yNFH6wjGekRUW6Dfgyg4J4h6wKKdF
```

**Monero (XMR):**
```
46uV2fMZT3EWkBrGUgszJCcbqFqEvqrB4bZBJwsbx7yA8e2WBakXzJSUK8aqT4GoqERzbg4oKT2SiPeCgjzVH6VpSQ5y7KQ
```

## License

@plaindb/log is [MIT licensed](https://gitlab.com/frenware/framework/plaindb/log/-/blob/master/LICENSE).
