const { _, log, Pipe } = require('basd')
// const { Readable } = require('stream')
// ... rest of the class


/**
 * Log class that acts as a wrapper for a database.
 */
class Log {
  /**
   * Creates a new Log instance.
   * @param {object} db - The database object.
   */
  constructor(db) {
    _.objProp(this, 'db', db)
    this.length = 0
    this.db.setOpts({ keyEncoding: 'lexint' })
  }

  /**
   * Starts a new Log instance.
   * @param {...*} args - The arguments to pass to the constructor.
   * @returns {Promise<Log>} The started Log instance.
   */
  static start(...args) {
    const obj = new this(...args)
    return obj.start().then(() => obj)
  }

  /**
   * Starts the Log instance.
   * @returns {Promise<void>}
   */
  async start() {
    await this.db.isReady()
    this.length = await this.height()
  }

  /**
   * Returns the height of the Log.
   * @returns {Promise<number>}
   */
  height() {
    return this.head().then(seq => seq + 1)
  }

  /**
   * Returns the sequence number of the last entry.
   * @returns {Promise<number>}
   */
  head() {
    return this.db.collect('key', { reverse: true, limit: 1 })
      .then(arr => !_.isNil(arr[0]) ? arr[0] : -1)
  }

  /**
   * Appends a value to the Log.
   * @param {*} value - The value to append.
   * @returns {Promise<number>} The sequence number of the appended entry.
   */
  append(value) {
    return this.push(value)
  }

  /**
   * Pushes a value into the Log.
   * @param {*} value - The value to push.
   * @returns {Promise<number>} The sequence number of the pushed entry.
   */
  push(value) {
    return this.head().then(seq => {
      seq++
      return this.db.put(seq, value).then(() => {
        this.length = seq + 1
        return seq
      })
    })
  }

  /**
   * Shifts the first value from the Log.
   * @returns {Promise<*>} The shifted value.
   */
  async shift() {
    let seq = await this.head()
    let first = await this.db.get(0)
    await this.db.del(0)
    for (let ii = 0; ii < seq; ii++) {
      let index = ii + 1
      let value = await this.db.get(index)
      await this.db.put(ii, value)
      await this.db.del(index)
    }
    this.length = await this.height()
    return first
  }

  /**
   * Pops the last value from the Log.
   * @returns {Promise<*>} The popped value.
   */
  async pop() {
    const seq = await this.head()
    const value = await this.get(seq)
    await this.db.del(seq)
    this.length = await this.height()
    return value
  }

  /**
   * Splices the Log.
   * @param {number} start - The starting index.
   * @param {number} delCount - The number of elements to delete.
   * @param {...*} items - The items to insert.
   * @returns {Promise<void>}
   */
  async splice(start, delCount = 0, ...items) {
    let len = await this.height()
    const toDelete = []
    if (delCount > 0) {
      for (let ii = start; ii < Math.min(start + delCount, len); ii++) {
        toDelete.push(await this.db.get(ii))
      }
    }
    let shiftStart = start + delCount
    for (let ii = shiftStart; ii < len; ii++) {
      const val = await this.db.get(ii)
      await this.db.put(ii - delCount, val)
    }
    for (let ii = len - 1; ii >= len - delCount; ii--) {
      await this.db.del(ii)
    }
    let shiftEnd = len - delCount
    for (let ii = shiftEnd - 1; ii >= start; ii--) {
      const val = await this.db.get(ii)
      await this.db.put(ii + items.length, val)
    }
    for (let ii = 0; ii < items.length; ii++) {
      await this.db.put(start + ii, items[ii])
    }
    this.length = await this.height()
  }

  /**
   * Gets the value at a specific sequence number.
   * @param {number} seq - The sequence number.
   * @returns {Promise<*>} The value.
   */
  get(seq) {
    return this.db.get(seq)
  }

  /**
   * Returns an iterator over the Log.
   * @param {object} opts - The options for the iterator.
   * @returns {AsyncGenerator} The iterator.
   */
  async *iterator(opts = {}) {
    for (const key of ['gt', 'gte', 'lt', 'lte']) {
      if (!_.isNumeric(opts[key]))
        continue
      if (!_.isString(opts[key]))
        opts[key] = _.lexint.pack(opts[key], 'hex')
    }
    for await (const item of this.db.iterator(opts))
      yield item
  }

  /**
   * Lists the entries in the Log.
   * @param {object} opts - The options for listing.
   * @returns {Promise<Array>} The list of entries.
   */
  list(opts = {}) {
    opts = _.defaults(opts, {
      reverse: opts.reverse,
      limit: opts.limit,
      valuesOnly: null,
    })
    if (opts.valuesOnly)
      return this.db.collect('value', opts)
    return this.db.collect('read', opts)
      .then(list => list.map(({ key, value }) => ({ seq: key, value })))
  }

  /**
   * Streams the entries between start and end sequence numbers.
   * @param {number} start - The start sequence number.
   * @param {number} end - The end sequence number.
   * @param {string} type - The type of stream.
   */
  stream(start, end, type = 'value') {
    const opts = {
      gt: start - 1,
      lt: end + 1
    }
    const self = this
    return new Pipe({
      async read () {
        for await (const [key, value] of self.iterator(opts)) {
          this.push(type === 'value' ? value : [key, value])
        }
        this.push(null)
      }
    })
    return stream
  }
}

module.exports = Log
