
interface LogOptions {
  reverse?: boolean;
  limit?: number;
  valuesOnly?: boolean;
  gt?: number | string;
  gte?: number | string;
  lt?: number | string;
  lte?: number | string;
}

interface LogItem {
  key: number;
  value: any;
}

declare class Log {
  constructor(db: any);
  static start(...args: any[]): Promise<Log>;
  start(): Promise<void>;
  height(): Promise<number>;
  head(): Promise<number>;
  append(value: any): Promise<number>;
  push(value: any): Promise<number>;
  shift(): Promise<any>;
  pop(): Promise<any>;
  splice(start: number, delCount?: number, ...items: any[]): Promise<void>;
  get(seq: number): Promise<any>;
  iterator(opts?: LogOptions): AsyncGenerator<LogItem, void, undefined>;
  list(opts?: LogOptions): Promise<LogItem[]>;
  stream(start: number, end: number, type?: string): void;  // Implement this as needed
  length: number;
}

export = Log;
