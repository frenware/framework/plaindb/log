<a name="Log"></a>

## Log
Log class that acts as a wrapper for a database.

**Kind**: global class  

* [Log](#Log)
    * [new Log(db)](#new_Log_new)
    * _instance_
        * [.start()](#Log+start) ⇒ <code>Promise.&lt;void&gt;</code>
        * [.height()](#Log+height) ⇒ <code>Promise.&lt;number&gt;</code>
        * [.head()](#Log+head) ⇒ <code>Promise.&lt;number&gt;</code>
        * [.append(value)](#Log+append) ⇒ <code>Promise.&lt;number&gt;</code>
        * [.push(value)](#Log+push) ⇒ <code>Promise.&lt;number&gt;</code>
        * [.shift()](#Log+shift) ⇒ <code>Promise.&lt;\*&gt;</code>
        * [.pop()](#Log+pop) ⇒ <code>Promise.&lt;\*&gt;</code>
        * [.splice(start, delCount, ...items)](#Log+splice) ⇒ <code>Promise.&lt;void&gt;</code>
        * [.get(seq)](#Log+get) ⇒ <code>Promise.&lt;\*&gt;</code>
        * [.iterator(opts)](#Log+iterator) ⇒ <code>AsyncGenerator</code>
        * [.list(opts)](#Log+list) ⇒ <code>Promise.&lt;Array&gt;</code>
        * [.stream(start, end, type)](#Log+stream)
    * _static_
        * [.start(...args)](#Log.start) ⇒ [<code>Promise.&lt;Log&gt;</code>](#Log)

<a name="new_Log_new"></a>

### new Log(db)
Creates a new Log instance.


| Param | Type | Description |
| --- | --- | --- |
| db | <code>object</code> | The database object. |

<a name="Log+start"></a>

### log.start() ⇒ <code>Promise.&lt;void&gt;</code>
Starts the Log instance.

**Kind**: instance method of [<code>Log</code>](#Log)  
<a name="Log+height"></a>

### log.height() ⇒ <code>Promise.&lt;number&gt;</code>
Returns the height of the Log.

**Kind**: instance method of [<code>Log</code>](#Log)  
<a name="Log+head"></a>

### log.head() ⇒ <code>Promise.&lt;number&gt;</code>
Returns the sequence number of the last entry.

**Kind**: instance method of [<code>Log</code>](#Log)  
<a name="Log+append"></a>

### log.append(value) ⇒ <code>Promise.&lt;number&gt;</code>
Appends a value to the Log.

**Kind**: instance method of [<code>Log</code>](#Log)  
**Returns**: <code>Promise.&lt;number&gt;</code> - The sequence number of the appended entry.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to append. |

<a name="Log+push"></a>

### log.push(value) ⇒ <code>Promise.&lt;number&gt;</code>
Pushes a value into the Log.

**Kind**: instance method of [<code>Log</code>](#Log)  
**Returns**: <code>Promise.&lt;number&gt;</code> - The sequence number of the pushed entry.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to push. |

<a name="Log+shift"></a>

### log.shift() ⇒ <code>Promise.&lt;\*&gt;</code>
Shifts the first value from the Log.

**Kind**: instance method of [<code>Log</code>](#Log)  
**Returns**: <code>Promise.&lt;\*&gt;</code> - The shifted value.  
<a name="Log+pop"></a>

### log.pop() ⇒ <code>Promise.&lt;\*&gt;</code>
Pops the last value from the Log.

**Kind**: instance method of [<code>Log</code>](#Log)  
**Returns**: <code>Promise.&lt;\*&gt;</code> - The popped value.  
<a name="Log+splice"></a>

### log.splice(start, delCount, ...items) ⇒ <code>Promise.&lt;void&gt;</code>
Splices the Log.

**Kind**: instance method of [<code>Log</code>](#Log)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| start | <code>number</code> |  | The starting index. |
| delCount | <code>number</code> | <code>0</code> | The number of elements to delete. |
| ...items | <code>\*</code> |  | The items to insert. |

<a name="Log+get"></a>

### log.get(seq) ⇒ <code>Promise.&lt;\*&gt;</code>
Gets the value at a specific sequence number.

**Kind**: instance method of [<code>Log</code>](#Log)  
**Returns**: <code>Promise.&lt;\*&gt;</code> - The value.  

| Param | Type | Description |
| --- | --- | --- |
| seq | <code>number</code> | The sequence number. |

<a name="Log+iterator"></a>

### log.iterator(opts) ⇒ <code>AsyncGenerator</code>
Returns an iterator over the Log.

**Kind**: instance method of [<code>Log</code>](#Log)  
**Returns**: <code>AsyncGenerator</code> - The iterator.  

| Param | Type | Description |
| --- | --- | --- |
| opts | <code>object</code> | The options for the iterator. |

<a name="Log+list"></a>

### log.list(opts) ⇒ <code>Promise.&lt;Array&gt;</code>
Lists the entries in the Log.

**Kind**: instance method of [<code>Log</code>](#Log)  
**Returns**: <code>Promise.&lt;Array&gt;</code> - The list of entries.  

| Param | Type | Description |
| --- | --- | --- |
| opts | <code>object</code> | The options for listing. |

<a name="Log+stream"></a>

### log.stream(start, end, type)
Streams the entries between start and end sequence numbers.

**Kind**: instance method of [<code>Log</code>](#Log)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| start | <code>number</code> |  | The start sequence number. |
| end | <code>number</code> |  | The end sequence number. |
| type | <code>string</code> | <code>&quot;value&quot;</code> | The type of stream. |

<a name="Log.start"></a>

### Log.start(...args) ⇒ [<code>Promise.&lt;Log&gt;</code>](#Log)
Starts a new Log instance.

**Kind**: static method of [<code>Log</code>](#Log)  
**Returns**: [<code>Promise.&lt;Log&gt;</code>](#Log) - The started Log instance.  

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | The arguments to pass to the constructor. |

